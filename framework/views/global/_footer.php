<?php

// =============================================================================
// VIEWS/GLOBAL/_FOOTER.PHP
// -----------------------------------------------------------------------------
// Includes the wp_footer() hook and closes out the <body> and <html> tags.
// =============================================================================

?>

  <?php do_action( 'x_before_site_end' ); ?>

  </div> <!-- END #top.site -->

  <?php do_action( 'x_after_site_end' ); ?>

<?php wp_footer(); ?>

<script>
	
jQuery('.menu-item').hover(function() {
	var sub = jQuery(this).children('.sub-menu');
	if(!sub) {
		return false;
	}
	sub = sub[0];

	if(!sub) {
		return false;
	}
	setTimeout(function(){
		// jQuery(this).addClass('x-active');
		var margin = jQuery(window).height() - sub.getBoundingClientRect().bottom - 10;
		if (margin < 0) {
			jQuery(sub).css('margin-top', margin);
		}
	}, 400);
});
</script>
</body>
</html>