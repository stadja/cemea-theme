<?php

class Ateliers {
	static $atelierCache = array();

	static function getAteliersByFormationId($formationId) {
		if (!isset(self::$atelierCache[$formationId])) {
			global $wpdb;
			$format = 'Ymd';

			$today = date($format);
			$dates = $wpdb->get_results( "SELECT * ".
					" FROM  `cemea_postmeta`".
					" WHERE  `meta_key` like 'ateliers_%_date'".
					" AND  `meta_value` >= ".$today.
					" AND  `post_id` = ".$formationId.
					" order by `meta_value` ASC", OBJECT );

			$ateliers = array();
			foreach ($dates as $date) {
				$atelier = array();

				$key = $date->meta_key;
				$date = $date->meta_value;

				$matches = array();
				preg_match('/ateliers_(..?)_date/', $key, $matches);
				$atelierId = $matches[1];

				$ateliers[] = self::getSpectificAteliersByFormationId($formationId, $atelierId);
			}
			self::$atelierCache[$formationId] = $ateliers;
		}

		return self::$atelierCache[$formationId];
	}

	static function getSpectificAteliersByFormationId($formationId, $atelierId) {
		global $wpdb;

		$atelier = array();
		$atelier['formationId'] = $formationId;
		$atelier['atelierId'] = $atelierId;

		$format = 'Ymd';
		$date = $wpdb->get_results( "SELECT * ".
			" FROM  `cemea_postmeta`".
			" WHERE  `meta_key` = 'ateliers_".$atelierId."_date'".
			" AND  `post_id` = ".$formationId, OBJECT );
		$date = $date[0];
		$atelier['date'] = DateTime::createFromFormat($format, $date->meta_value);

		$dateEnd = $wpdb->get_results( "SELECT * ".
			" FROM  `cemea_postmeta`".
			" WHERE  `meta_key` = 'ateliers_".$atelierId."_date_de_fin'".
			" AND  `post_id` = ".$formationId, OBJECT );
		$dateEnd = $dateEnd[0];
		$atelier['dateEnd'] = DateTime::createFromFormat($format, $dateEnd->meta_value);
		if (!$dateEnd->meta_value) {
			$atelier['dateEnd'] = $atelier['date'];
		}

		$heureStart = $wpdb->get_results( "SELECT * ".
			" FROM  `cemea_postmeta`".
			" WHERE  `meta_key` = 'ateliers_".$atelierId."_heure_de_debut'".
			" AND  `post_id` = ".$formationId, OBJECT);
		$heureStart = $heureStart[0];
		$atelier['heureStart'] = DateTime::createFromFormat('U', $heureStart->meta_value);

		$heureEnd = $wpdb->get_results( "SELECT * ".
			" FROM  `cemea_postmeta`".
			" WHERE  `meta_key` = 'ateliers_".$atelierId."_heure_de_fin'".
			" AND  `post_id` = ".$formationId, OBJECT);
		$heureEnd = $heureEnd[0];
		$atelier['heureEnd'] = DateTime::createFromFormat('U', $heureEnd->meta_value);

		$location = $wpdb->get_results( "SELECT * ".
			" FROM  `cemea_postmeta`".
			" WHERE  `meta_key` = 'ateliers_".$atelierId."_autre_lieu'".
			" AND  `post_id` = ".$formationId, OBJECT );
		$location = $location[0];
		$atelier['location'] = $location->meta_value;
		if (!$location->meta_value) {
			$atelier['location'] = 'Cemea Genève, route des Franchises 11, 1203 Genève';
		}

		return new Atelier($atelier);
	}
}

class Atelier {
	public function __construct($data) {
		foreach ($data as $key => $value) {
			$this->$key = $value;
		}
		return $this;
	}

	public function isFull() {
		return false;
	}

	public function getLocation() {
		return $this->location;
	}

	public function getStartDate($format = 'Ymd') {
		return $this->date->format($format);
	}

	public function getDateString() {
		if (!$this->date) {
			return '';
		}
		if ($this->date == $this->dateEnd) {
			return 'Le '.$this->date->format('d/m/Y');
		}
			
		if ($this->date->format('Y') != $this->dateEnd->format('Y')) {
			return 'Du '.$this->date->format('d/m/Y').' au '.$this->dateEnd->format('d/m/Y');
		}
			
		if ($this->date->format('m') != $this->dateEnd->format('m')) {
			return 'Du '.$this->date->format('d/m').' au '.$this->dateEnd->format('d/m/Y');
		}
			
		return 'Du '.$this->date->format('d').' au '.$this->dateEnd->format('d/m/Y');
	}

	public function getHoraireString() {
		if (!$this->heureStart || !$this->heureEnd) {
			return '';
		}
		return 'De '.$this->heureStart->format('H:i').' à '.$this->heureEnd->format('H:i');
	}

	/**
	 * Return the admin url of this atelier
	 * 			
	 * @return void the admin url of this atelier
	 */
	public function  getAdminUrl()
	{
	    $url = get_admin_url().'post.php?post='.$this->formationId.'&action=edit#acf-field_558d6d3ab2eb7-'.$this->atelierId;
	    return $url; /* the admin url of this atelier */
	}

	public function getFormationId() {
		return $this->formationId;
	}
	
}